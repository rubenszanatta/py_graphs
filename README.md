# py_graphs

### Data vis using pyhton

### Dependencies:

* python3
* matplotlib (pip)
* pandas (pip)

### Usage:

You can run it as

	./data.py yourCSVfile.csv

or

	pyhton3 data.py yourCSVfile.csv

Insert the columns you wish to plot on the INPUT constant on line 18 with the same name stated on the first line of the CSV database.

Warning: Figure 3 is still hardcoded and might break (blank print) if names are changed.
