#!/usr/bin/env python3

#pip install numpy matplotlib
#Referencia:
#https://matplotlib.org/stable/gallery/text_labels_and_annotations/date.html#sphx-glr-gallery-text-labels-and-annotations-date-py
#https://stackoverflow.com/questions/14762181/adding-a-y-axis-label-to-secondary-y-axis-in-matplotlib



#----------------------------------------------------------------
# CONSTANTES
#----------------------------------------------------------------
fig1title = 'Figura 1 - # de Produções e Grupos'
fig2title = 'Figura 2 - # de Produções e Grupos'
fig3title = 'Figura 3 - # de Produções e Grupos'

#Labels dos gráficos a se plotar, deve ser igual ao CSV
INPUT = ["Prods","Grupos","Teste"]

#Separador do CSV, depende do OFFICE Suite
CSVSEPARATOR = ','

#----------------------------------------------------------------
# Dependencias:
#	matplotlib
#	pandas
#----------------------------------------------------------------

import sys

import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import matplotlib.dates as mdates

import pandas as pd


#----------------------------------------------------------------
# leitura dos dados e criação do dataFrame array
#----------------------------------------------------------------

print("Your current input is: ",INPUT,"\n")

try:
	datapath = sys.argv[1]
	#loads data to msft object
	msft = pd.read_csv(datapath, sep=CSVSEPARATOR, parse_dates=[0])
except Exception as e:
	print(e)
	print("\nSomething went wrong when reading or calling the CSV file. \n")
	sys.exit()

# seeks "Data" - DATE column, as it is needed for the graphs
csvlabels = ["empty"]
for index, label in enumerate(msft.columns):
	if label == "Data":
		print("Data(Dates) column found ...\n")
		csvlabels = msft.columns.delete(index)
		#creates a copy of labels withou the Data label
try:
	if csvlabels == ["empty"]:
		print("'Data' columns NOT found, please check the CSV\n")
		sys.exit()
except:
	pass

#Compares the INPUT with the csvlabels
plotlabels = [ label for label in INPUT if label in csvlabels ]
if len(plotlabels) != 0:
	print("Plotting:\n",plotlabels,"\n")
else:
	print("INPUT Labels not found in CSV\n")
	sys.exit()


#----------Figura 1------------------------------------------------
#Plot em linha c/ marcas em escala única
#-----------------------------------------------------------------

fig1, ax1 = plt.subplots()
fig1.suptitle(fig1title)

for label in plotlabels:
    if label != "Data":
        ax1.plot("Data",label, data=msft, marker='o')

ax1.legend()
ax1.grid(True)


#----------Figura 2------------------------------------------------
# Plot em linha c/ marcas, EM DOIS OU MAIS SUBPLOTS, apenas um levanta a excecao
#------------------------------------------------------------------
try:
	fig2, axs = plt.subplots(len(plotlabels))
	fig2.suptitle(fig2title)

	for index, label in enumerate(plotlabels):
		if label != "Data":
			axs[index].plot("Data",label, data=msft, marker='o')
			axs[index].grid(True)
			axs[index].legend()


except:
	print("FIG2 ERROR: Somthing broke here. You need 2 or more labels on input for this to work\n")

#----------Figura 3------------------------------------------------

# Plot em linha c/ marcas, em duas escalas
# "Zeros desalinhados"
# gráfico pode ser usado se os primeiros elementos coincidirem
# ver comentario do sigur na ref1
#------------------------------------------------------------------
try:

	fig3, axsDoubleLeft = plt.subplots()

	axsDoubleLeft.plot("Data","Prods", data=msft, color='orange', marker='o')
	axsDoubleLeft.grid(True)

	axsDoubleRight = axsDoubleLeft.twinx()
	axsDoubleRight.plot("Data","Grupos", data=msft, color='green', marker='o')
	#axsDoubleRight.grid(True)

	#Labels
	axsDoubleLeft.set_xlabel('Data')
	axsDoubleLeft.set_ylabel('Prods'  ,color='orange')
	axsDoubleRight.set_ylabel('Grupos',color='green')
	axsDoubleRight.legend(loc=2) #upper left
	axsDoubleLeft.legend(loc=4)  #lower right
	# Rotates and right aligns the x labels, and moves the bottom of the
	# axes up to make room for them.
	fig3.autofmt_xdate()
	#fig3.legend()
	fig3.suptitle(fig3title)

except:
	print("FIG3 ERROR: Prods and Grupos were not found on the CSV\n")

#------------------------------------------------------------------
# Mostrar Figuras
#------------------------------------------------------------------
plt.show()
